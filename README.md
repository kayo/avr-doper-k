Hacking guide
=============

Bootloader
----------

```
cd bootloader/firmware
make clean elf
# Attach ISP flasher and flash bootloader
make flash
```

Firmware
--------

```
cd firmware
make clean doper-k
# Flash fuse bytes
make fuse
# Detach ISP flasher
# put jumper
# and attach USB plug
# flash firmware using bootloader
make flash
```
